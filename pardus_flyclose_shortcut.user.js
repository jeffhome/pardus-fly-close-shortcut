// ==UserScript==
// @name        Starbase Flyclose Shortcut
// @namespace   http://userscripts.xcom-alliance.info/
// @description Allows a keypress to fly close and exit fly close on a starbase
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.01
// @include     http*://*.pardus.at/main.php*
// @include     http*://*.pardus.at/starbase.php
// @exclude     http*://chat.pardus.at*
// @exclude     http*://forum.pardus.at*
// @updateURL 	http://userscripts.xcom-alliance.info/flyclose_shortcut/pardus_flyclose_shortcut.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/flyclose_shortcut/pardus_flyclose_shortcut.user.js
// @icon 		http://userscripts.xcom-alliance.info/flyclose_shortcut/icon.png
// ==/UserScript==

/*****************************************************************************************
	Version Information

	2/09/2012 (Version 1.0)
	  - Released this simple keyboard shortcut

	2/09/2012 (Version 1.01)
	  - Minor problem with include matching resolved. Chrome & Firefox now supported

	When on a starbase, press "d" once to fly close and then again when flying close to
	exit the inner starbase. You can change this key choice by my updating the 
	KEY_PRESS_FOR_FLYCLOSE setting directly below.

*****************************************************************************************/

var KEY_PRESS_FOR_FLYCLOSE = 'd';

/*****************************************************************************************
	There is no need to modify anything further in this file
*****************************************************************************************/

function handleKeyPressForFlyClose(e) {
	if (e.ctrlKey || e.target.nodeName == 'INPUT' || e.target.nodeName == 'TEXTAREA') return;
	if (String.fromCharCode(e.which).toLowerCase() == KEY_PRESS_FOR_FLYCLOSE.toLowerCase()) {
		if (document.getElementById('aCmdExitSb')) {
			document.location = 'main.php?exitsb=1';
		} else {
			document.location = 'main.php?entersb=1';
		}
	}
};
window.addEventListener("keypress", handleKeyPressForFlyClose);


