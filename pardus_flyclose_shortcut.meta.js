// ==UserScript==
// @name        Starbase Flyclose Shortcut
// @namespace   http://userscripts.xcom-alliance.info/
// @description Allows a keypress to fly close and exit fly close on a starbase
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.01
// @include     http*://*.pardus.at/main.php*
// @include     http*://*.pardus.at/starbase.php
// @exclude     http*://chat.pardus.at*
// @exclude     http*://forum.pardus.at*
// @updateURL 	http://userscripts.xcom-alliance.info/flyclose_shortcut/pardus_flyclose_shortcut.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/flyclose_shortcut/pardus_flyclose_shortcut.user.js
// @icon 		http://userscripts.xcom-alliance.info/flyclose_shortcut/icon.png
// ==/UserScript==
